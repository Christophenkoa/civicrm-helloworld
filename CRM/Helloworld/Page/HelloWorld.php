<?php
use CRM_Helloworld_ExtensionUtil as E;

class CRM_Helloworld_Page_HelloWorld extends CRM_Core_Page {

  public function run() {
    // Example: Set the page-title dynamically; alternatively, declare a static title in xml/Menu/*.xml
    CRM_Utils_System::setTitle(E::ts('HelloWorld'));

    // Example: Assign a variable for use in a template
    $this->assign('helloWorld', "Hello world");

    parent::run();
  }

}
